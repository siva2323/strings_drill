let string2=(input) =>
{
    let regexExp = (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/);
    if(regexExp.test(input)) regexExp=input.split(".");
    else return [];
    let answer=[]
    regexExp.filter((index) =>{answer.push(+index);})
    return answer;
}
module.exports=string2;
