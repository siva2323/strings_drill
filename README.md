# Strings_drill

==== String Problem #1 ====
- There are numbers that are stored in the format "$100.45", "$1,002.22", "-$123", and so on. Write a function to convert the given strings into their equivalent numeric format without any precision loss - 100.45, 1002.22, -123 and so on. There could be typing mistakes in the string so if the number is invalid, return 0. 


// ==== String Problem #2 ====
- Given an IP address - "111.139.161.143". Split it into its component parts 111, 139, 161, 143 and return it in an array in numeric values. [111, 139, 161, 143].

- Support IPV4 addresses only. If there are other characters detected, return an empty array.


// ==== String Problem #3 ====
- Given a string in the format of "10/1/2021", print the month in which the date is present in.


// ==== String Problem #4 ====
- Given an object in the following format, return the full name in title case.
- {"first_name": "JoHN", "last_name": "SMith"}
- {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}


// ==== String Problem #5 ====
- Given an array of strings ["the", "quick", "brown", "fox"], convert it into a string "the quick brown fox."
- If the array is empty, return an empty string.
